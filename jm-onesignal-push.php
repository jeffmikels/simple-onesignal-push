<?php
/*
Plugin Name: Simple OneSignal Mobile Push
Plugin URI: https://gitlab.com/jeffmikels/simple-onesignal-push
Description: This is a super simple implementation of Mobile Push Notifications using OneSignal.com
Author: Jeff Mikels
Text Domain: jm-onesignal-push
Version: 0.1
Author URI: http://jeff.mikels.cc
*/

// WORDPRESS MODIFICATION FUNCTIONS
function jmo_install()
{
	$role = get_role( 'administrator' );
	$role->add_cap('jmo_admin');
	remove_role('jmo_admin');
}
function jmo_uninstall()
{
	$role = get_role( 'administrator' );
	$role->remove_cap('jmo_admin');
	remove_role('jmo_admin');
}
register_activation_hook(__FILE__, 'jmo_install');
register_deactivation_hook(__FILE__, 'jmo_uninstall');


/* Add Administration Pages */
if ( is_admin() )
{
	add_action ('admin_menu', 'jmo_admin_menu');
	add_action ('admin_init', 'jmo_register_options');
}
function jmo_admin_menu()
{
	add_options_page('Simple OneSignal', 'Simple OneSignal', 'publish_posts', 'jmo-options', 'jmo_options_page');
}
function jmo_register_options()
{
	register_setting('jmo_options','jmo_options'); // one group to store all options as an array
}
function jmo_options_page()
{
	if ( !current_user_can('publish_posts') ) wp_die( __('You do not have sufficient permissions to access this page.' ) );

	// if (isset($_GET['settings-updated']) && $_GET['settings-updated']) flush_rewrite_rules();

	$options = Array(
		'onesignal_app_id'=>Array(
			'type'=>'text',
			'label'=>'OneSignal App ID',
			'value'=>'',
			'description'=>'',
			'admin_only' => 0
		),
		'onesignal_rest_key'=>Array(
			'type'=>'text',
			'label'=>'OneSignal REST Key',
			'value'=>'',
			'description'=>'',
			'admin_only' => 0
		),
		'auto_send_post_types'=>Array(
			'type'=>'text',
			'label'=>'Automatically send notifications on these post types.',
			'value'=>'post',
			'description'=>'A push notification will be sent whenever one of these post types is published. Enter a comma-separated list.',
			'admin_only' => 0
		),
		'android_icon'=>Array(
			'type'=>'text',
			'label'=>'Android Icon Resource',
			'value'=>'ic_stat_onesignal_default',
			'description'=>'Enter the name of a drawable resource available in your app. (e.g. ic_stat_notify)',
			'admin_only' => 0
		),
		/*
		'ios'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Push to iOS Devices',
			'value'=>0,
			'description'=>'Apple iOS must be set up as a platform in your OneSignal account for this to work.',
			'admin_only' => 0
		),
		'android'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Push to Android Devices',
			'value'=>0,
			'description'=>'Android must be set up as a platform in your OneSignal account for this to work.',
			'admin_only' => 0
		),
		'win8'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Push to Windows Phone 8.0 Devices',
			'value'=>0,
			'description'=>'Windows Phone 8.0 must be set up as a platform in your OneSignal account for this to work.',
			'admin_only' => 0
		),
		'win81'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Push to Windows Phone 8.1 Devices',
			'value'=>0,
			'description'=>'Windows Phone 8.1 must be set up as a platform in your OneSignal account for this to work.',
			'admin_only' => 0
		),
		'fire'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Push to Amazon Fire Devices',
			'value'=>0,
			'description'=>'Amazon Fire must be set up as a platform in your OneSignal account for this to work.',
			'admin_only' => 0
		),*/
	);


	// populate values from those stored in the database
	$stored_options = get_option('jmo_options');
	foreach ($options as $key=>$value)
	{
		$options[$key]['value'] = $stored_options[$key];
	}
	?>

	<div class="wrap">
		<h2>Simple OneSignal Mobile Push</h2>

		<?php if (current_user_can('publish_posts')): ?>

		<form method="post" action="options.php">
			<?php settings_fields('jmo_options'); ?>

			<table class="form-table">
				<?php foreach ($options as $key=>$value): ?>

					<?php if (($value['admin_only'] == 1) && (! current_user_can('jmo_admin'))) continue; ?>

					<tr valign="top">
						<th scope="row"><?php echo $value['label']; ?><?php if ($value['admin_only'] == 1):?><br />ADMIN ONLY<?php endif; ?></th>
						<td>
							<?php if ($value['type'] == 'checkbox') : ?>
							<input name="jmo_options[<?php echo $key; ?>]" type="checkbox" value="<?php echo htmlentities($value['checkvalue']); ?>" <?php checked('1', $value['value']); ?> /> <?php echo $value['label']; ?>
							<?php elseif ($value['type'] == 'text') : ?>
							<input style="width:60%;" name="jmo_options[<?php echo $key; ?>]" type="text" value="<?php echo htmlentities($value['value']); ?>" />
							<?php elseif ($value['type'] == 'disabled') : ?>
							<input style="width:60%;"
								name="jmo_options[<?php echo $key; ?>]"
								type="text"
								disabled="disabled"
								value="<?php echo htmlentities($value['value']); ?>"
								/>
							<?php else: ?>
							<input
								style="width:60%;"
								name="jmo_options[<?php echo $key; ?>]"
								type="<?php echo $value['type']; ?>"
								value="<?php echo htmlentities($value['value']); ?>"
								/>
							<?php endif; ?>
							<p><small><?php echo $value['description']; ?></small></p>
						</td>
					</tr>

				<?php endforeach; ?>
				
			</table>

			<?php submit_button(); ?>

		</form>
		
		<script type="text/javascript">
			function jmo_check_submit()
			{
				console.log('preparing to submit');
				
				// check to see if form has all required fields
				var title = jQuery('#jmo_now_title').val();
				var subtitle = jQuery('#jmo_now_subtitle').val();
				var message = jQuery('#jmo_now_message').val();
				var url = jQuery('#jmo_now_url').val();
				var testing = jQuery('#jmo_now_test')[0].checked ? "true" : "false";
				var ready = jQuery('#jmo_now_confirm')[0].checked ? "true" : "false";
				
				if (ready && title && message)
				{
					var data = {
						action: 'jmo_ajax_notify',
						jmo_now_title: title,
						jmo_now_subtitle: subtitle,
						jmo_now_message: message,
						jmo_now_url: url,
						jmo_now_test: testing,
						jmo_now_ready: ready,
					};
					console.log(data);
					
					jQuery.post(ajaxurl, data, function(res){
						console.log(res);
						var plural = 's';
						if (res.recipients == 1) plural = '';
						if (res.recipients > 0) jQuery('#jmo_alert').html('Sent to ' + res.recipients + ' recipient' + plural + '.');
						else jQuery('#jmo_alert').html('FAILED TO SEND<br />ERRORS:<br />' + res.errors.join('<br />'));
					}, 'json');
					
					jQuery('#jmo_now_form').hide();
					jQuery('#jmo_alert').html('SENDING...');
				}
				else
				{
					jQuery('#jmo_alert').html('You must include a title, message, and check the "Are You Sure?" box.');
				}
			}
		</script>
		
		<div class="jmo_alert" style="color:red; text-transform:uppercase;" id="jmo_alert"></div>
		<form action="admin-post.php" method="post" onsubmit="jmo_check_submit(); return false;" id="jmo_now_form">
			<h2>Send Immediate Message</h2>
			<p>Please don't overuse this feature!</p>
			<table class="form-table">
				<tr valign="top">
					<th scope="row">Title:</th>
					<td><input style="width:60%;" type="text" id="jmo_now_title" value="" /></td>
				</tr>
				<tr valign="top">
					<th scope="row">iOS 10 Subtitle:</th>
					<td><input style="width:60%;" type="text" id="jmo_now_subtitle" value="" /></td>
				</tr>
				<tr valign="top">
					<th scope="row">Message:</th>
					<td><input style="width:60%;" type="text" id="jmo_now_message" value="" /></td>
				</tr>
				<tr valign="top">
					<th scope="row">URL:</th>
					<td><input style="width:60%;" type="text" id="jmo_now_url" value="" /><br /><small>The device will open a browser to this URL when the notification is clicked.</small></td>
				</tr>
				<tr valign="top">
					<th scope="row">Send to Testing Devices?</th>
					<td><input type="checkbox" id="jmo_now_test" checked="checked" /><br /><small>Send to devices in the "Test Devices" Segment Only</small></td>
				</tr>
				<tr valign="top">
					<th scope="row">Are You Sure?</th>
					<td><input type="checkbox" id="jmo_now_confirm" value="1" onClick="//return confirm('Are you sure?');" /><br /><small>You have to click this checkbox if you really want to send the notification.</small></td>
				</tr>
			</table>
			
			<input type="hidden" name="action" value="jmo_maybe_notify" />
			<?php submit_button('Send Notification Now'); ?>
			
		</form>

		<?php endif;?>
		
	</div>

	<?php
}

add_action( 'wp_ajax_jmo_ajax_notify', 'jmo_ajax_notify', 99 );
function jmo_ajax_notify()
{
	$res = jmo_maybe_notify();
	error_log($res);
	echo $res;
	wp_die();
}

add_action( 'admin_post_jmo_maybe_notify', 'jmo_maybe_notify', 99 );
function jmo_maybe_notify() {
	// generate notification
	$title = $_POST['jmo_now_title'];
	$subtitle = $_POST['jmo_now_subtitle'];
	$message = $_POST['jmo_now_message'];
	$url = $_POST['jmo_now_url'];
	$testing = empty($_POST['jmo_now_test']) ? 'false' : $_POST['jmo_now_test'];
	$ready = $_POST['jmo_now_ready'];
	$notification = array(
		'title' => $title,
		'subtitle' => $subtitle,
		'message' => $message,
		'testing' => $testing,
	);
	if (!empty($url)) $notification['url'] = $url;
	return jmo_send_notification($notification);
}

add_action('transition_post_status', 'jmo_publish_post', 99, $accepted_args=3);
function jmo_publish_post($new_status, $old_status, $post)
{
	
	if ($new_status != 'publish') return;
	if ($old_status == 'publish') return;
	
	// get the options
	$stored_options = get_option('jmo_options',array());
	
	if (empty($stored_options['onesignal_app_id']) || empty($stored_options['onesignal_rest_key']))
	{   
		jmo_err('OneSignal app is active but not set up. No notifications were sent.');
		return;
	}
	
	// check to see if post is one of the valid post types for notifications
	foreach (explode(',', $stored_options['auto_send_post_types']) as $post_type)
	{
		
		if (trim($post_type) == $post->post_type)
		{
			// generate notification
			$notification = array(
				'title' => 'New Content Published!',
				'subtitle' => '',
				'message' => $post->post_title,
				'url' => get_post_permalink($post->ID),
				'big_picture' => get_the_post_thumbnail_url($post)
			);
			jmo_msg('OneSignal push notifications were sent.');
			jmo_send_notification($notification);
			return;
		}
	}
}

function jmo_err($s)
{
	add_filter('redirect_post_location', function($location) use ($s) {
		return add_query_arg(array('jmo_err'=>urlencode($s)), $location);
	});
}
function jmo_msg($s)
{
	add_filter('redirect_post_location', function($location) use ($s) {
		return add_query_arg(array('jmo_msg'=>urlencode($s)), $location);
	});
}

add_action( 'admin_notices', 'jmo_notices' );
function jmo_notices()
{
	if (array_key_exists('jmo_err', $_GET))
	{
		
		?>

	    <div class="notice notice-error is-dismissible">
	        <p><?php echo $_GET['jmo_err'];?></p>
	    </div>

		<?php
	}

	if (array_key_exists('jmo_msg', $_GET))
	{
		
		?>

	    <div class="notice notice-success is-dismissible">
	        <p><?php echo $_GET['jmo_msg'];?></p>
	    </div>

		<?php
	}
}


// PLUGIN FUNCTIONS
function jmo_send_notification($n)
{
	// get the options
	$stored_options = get_option('jmo_options',array());
	
	$os_url = 'https://onesignal.com/api/v1/notifications';
	
	$fields = array(
		'app_id' => $stored_options['onesignal_app_id'],
		'included_segments' => array('All'),
		'contents' => array('en' => $n['message']),
		'headings' => array('en' => $n['title']),
		'subtitle' => array('en' => $n['subtitle']),
		'url' => $n['url'],
	);
	
	// This next line groups notifications together on android
	// it isn't useful unless you have a feature in the app to show
	// recent notifications
	// $fields['android_group'] = site_url(),
	
	if (!empty($n['testing']) && $n['testing'] == 'true')
		$fields['included_segments'] = array('Test Devices');
		
	
	// check for big_picture
	if (!empty($n['big_picture'])) $fields['big_picture'] = $n['big_picture'];
	
	// specify the icon resource
	if (!empty($stored_options['android_icon']))
		$fields['small_icon'] = $stored_options['android_icon'];
	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
											   'Authorization: Basic ' . $stored_options['onesignal_rest_key']));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	$response = curl_exec($ch);
	curl_close($ch);
	
	$return_data = json_decode($response, TRUE);
	$return_data['request'] = ['fields' => $fields];
	return json_encode($return_data);
}

?>
